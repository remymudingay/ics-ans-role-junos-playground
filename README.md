# ics-ans-role-junos-playground

Ansible role to test Juniper Junos switch configuration.

NOTE: This is not meant for production but only for testing scenarios.


## Role Variables

```yaml
# ping test server
junos_playground_ping_srv: IPADDRESS
# ping test router
junos_playground_ping_rt: IPADDRESS
junos_playground_network: []
  - interface: interface
    ip: IPADDRESS/MASK
...
```
```mermaid
graph TD;
  A[ServerA .1]-->|subnetA 172.21.1.0/24| B(.254 L3SwitchA .1);
  B-->|SubnetC 172.21.3.0/24| C(.254 L3SwitchB .2);
  D[Server .1]-->|subnetB_172.21.2.0/24| C;
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-junos-playground
```

## License

BSD 2-clause
