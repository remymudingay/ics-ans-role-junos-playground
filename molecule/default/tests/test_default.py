import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('molecule_group')


def test_default(host):
    assert os.system("zcat /config/juniper.conf.gz | grep 172.21")
